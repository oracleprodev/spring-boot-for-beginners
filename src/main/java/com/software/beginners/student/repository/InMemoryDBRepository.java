package com.software.beginners.student.repository;

import com.software.beginners.student.model.Student;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Repository
public class InMemoryDBRepository {

    private final List<Student> students = new ArrayList<>();

    public List<Student> select() {
        return students;
    }

    public Student selectByEmail(String email) {

        return students.stream().
                filter(student -> student.getEmail().equals(email))
                .findFirst()
                .orElse(null);
    }

    public Student save(Student student) {
        students.add(student);
        return student;
    }

    public Student update(Student student) {

        int position = IntStream.range(0, students.size())
                .filter(index -> students.get(index).getEmail().equals(student.getEmail()))
                .findFirst()
                .orElse(-1);

        if (position != -1) {
            students.set(position, student);
            return student;
        } else {
            return null;
        }
    }

    public void delete(String email) {

        Student student = selectByEmail(email);
        if (student != null) {
            students.remove(student);
        }
    }
}

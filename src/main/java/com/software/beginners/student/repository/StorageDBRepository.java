package com.software.beginners.student.repository;

import com.software.beginners.student.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StorageDBRepository extends JpaRepository<Student,Long> {

    Optional<Student> findByEmail(String email);

    void deleteByEmail(String email);
}

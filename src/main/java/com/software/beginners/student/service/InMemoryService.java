package com.software.beginners.student.service;

import com.software.beginners.student.general.GeneralService;
import com.software.beginners.student.model.Student;
import com.software.beginners.student.repository.InMemoryDBRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class InMemoryService implements GeneralService<Student> {

    private final InMemoryDBRepository inMemoryDBRepository;

    @Override
    public List<Student> select() {
        return inMemoryDBRepository.select();
    }

    @Override
    public Student selectByEmail(String email) {
        return inMemoryDBRepository.selectByEmail(email);
    }

    @Override
    public Student save(Student student) {
        return inMemoryDBRepository.save(student);
    }

    @Override
    public Student update(Student student) {
        return inMemoryDBRepository.update(student);
    }

    @Override
    public void delete(String email) {
        inMemoryDBRepository.delete(email);
    }
}

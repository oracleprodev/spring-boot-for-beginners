package com.software.beginners.student.service;

import com.software.beginners.student.general.GeneralService;
import com.software.beginners.student.model.Student;
import com.software.beginners.student.repository.StorageDBRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StorageDatabaseService implements GeneralService<Student> {

    private final StorageDBRepository storageDBRepository;

    @Override
    public List<Student> select() {
        return storageDBRepository.findAll();
    }

    @Override
    public Student selectByEmail(String email) {
        try {
            return storageDBRepository.findByEmail(email)
                    .orElseThrow(() ->
                            new FileNotFoundException("The Student not found"));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Student save(Student student) {
        return storageDBRepository.save(student);
    }

    @Override
    public Student update(Student student) {
        return storageDBRepository.save(student);
    }

    @Override
    public void delete(String email) {
        storageDBRepository.deleteByEmail(email);
    }
}

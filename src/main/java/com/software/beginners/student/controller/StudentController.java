package com.software.beginners.student.controller;

import com.software.beginners.student.general.GeneralService;
import com.software.beginners.student.model.Student;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {

    private final GeneralService<Student> generalService;

    public StudentController(@Qualifier("storageDatabaseService") GeneralService<Student> generalService) {
        this.generalService = generalService;
    }

    @GetMapping
    List<Student> select() {
        return generalService.select();
    }

    @GetMapping("/{email}")
    Student selectByEmail(@PathVariable String email) {
        return generalService.selectByEmail(email);
    }

    @PostMapping
    Student save(@RequestBody Student student) {
        return generalService.save(student);
    }

    @PutMapping
    Student update(@RequestBody Student student) {
        return generalService.update(student);
    }

    @DeleteMapping("/{email}")
    void delete(@PathVariable String email) {
        generalService.delete(email);
    }
}

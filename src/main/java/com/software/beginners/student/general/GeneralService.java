package com.software.beginners.student.general;

import java.util.List;

public interface GeneralService<T> {

    List<T> select();

    T selectByEmail(String email);

    T save(T t);

    T update(T t);

    void delete(String email);

}

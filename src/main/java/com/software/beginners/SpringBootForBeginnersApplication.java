package com.software.beginners;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootForBeginnersApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootForBeginnersApplication.class, args);
    }

}
